var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var kanaliZGeslom = [];
var kanaliBrezGesla = []


exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj',null);
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    vzdrzujSeznamUporabnikov(socket);
    obdelajZahtevoPoOsvezitvi(socket);//
    obdelajZasebnoSporocilo(socket); //!!!

    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  vzdrzujSeznamUporabnikov(socket);
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}



function pridruzitevKanalu(socket, kanal,passwd) {
  
  var napacnoGeslo;
  var prostodostopen;
  if(passwd !== null){
    //preveri če obstaja ze tak prostodosotopen kanal
    for(var i = 0; i < kanaliBrezGesla.length; i++){
      if(kanaliBrezGesla[i] === kanal){
        prostodostopen = true;  //vrni napako!!
      }
    }
    //preveri ce je ze tak kanal ustvajen
    var obstaja = false; 
    var index;
    for(var i = 0; i < kanaliZGeslom.length; i++){
      if(kanaliZGeslom[i].ime === kanal){
        index = i; 
        obstaja = true;
        break;
      }
    }
    if (!obstaja){ //treba ga je kreirati
      napacnoGeslo = false;
      var newChannel = {
        ime : kanal,
        geslo : passwd
      }
      kanaliZGeslom.push(newChannel);
    }
    else{
      if(kanaliZGeslom[index].geslo === passwd) { //ok
        napacnoGeslo = false;
      }
      else{
        napacnoGeslo = true;  //napacno geslo
      }
      //preveri ce je pravo geslo
      //ce je se lahko dzoina drugace javi napako
    }
  }
  else{ //kreiranje kanala brez gesla
    var problem = false;
      for(var i = 0; i < kanaliZGeslom.length; i++){
      if(kanaliZGeslom[i].ime === kanal){
        problem = true;
        break;
      }
    }
    if(!problem){
      kanaliBrezGesla.push(kanal);
      napacnoGeslo = false;
    }
    else
      napacnoGeslo = true;
  }
  
  if(napacnoGeslo){
     //vrni napako, da je napacno geeslo
     socket.emit('sporocilo', {
       besedilo: 'Pridružitev v kanal \"' + kanal + '\" ni bila uspešna, ker je geslo napačno!'
     });
    
  }
  else if (prostodostopen){
    //vrni da je prostodosopen 
    socket.emit('sporocilo', {
       besedilo: 'Izbrani kanal \"' + kanal + '\" je prosto dostopen in ne zahteva prijave z geslom,'+
            'zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom.'
     });
   
  }
  else{
    socket.leave(trenutniKanal[socket.id]);
    socket.join(kanal);
    trenutniKanal[socket.id] = kanal;
    socket.emit('pridruzitevOdgovor', {kanal: kanal,  vzdevek: vzdevkiGledeNaSocket[socket.id]});
    socket.broadcast.to(kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
    });
  
    var uporabnikiNaKanalu = io.sockets.clients(kanal);
    if (uporabnikiNaKanalu.length > 1) {
      var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        if (uporabnikSocketId != socket.id) {
          if (i > 0) {
            uporabnikiNaKanaluPovzetek += ', ';
          }
          uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];

        }
      }
      uporabnikiNaKanaluPovzetek += '.';
      socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
    }
  }

}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
        vzdrzujSeznamUporabnikov(socket);
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  vzdrzujSeznamUporabnikov(socket);
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', { //
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  vzdrzujSeznamUporabnikov(socket);
  socket.on('pridruzitevZahteva', function(kanal) {
    
    pridruzitevKanalu(socket, kanal.novKanal,kanal.geslo);
  });
}

function obdelajOdjavoUporabnika(socket) {
  vzdrzujSeznamUporabnikov(socket);
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}


function obdelajZahtevoPoOsvezitvi(socket){ //osvezevanje prejemnikov
  socket.on('osveziUpor',function(){
    vzdrzujSeznamUporabnikov(socket);
  });
}

function vzdrzujSeznamUporabnikov(socket){
    //najdi kanal tega socketa:
    var kanal = trenutniKanal[socket.id];
   
    var uporabniki= io.sockets.clients(kanal); //uporabniki ki so na kanalu na katerem je uporabnik s tem socket.id
    var uporabnikiNaKanalu = [] ;
    var leEden = false;
    if (uporabniki.length === 1) //smo sami na kanalu --- izpise se le nas vzdevek
      uporabnikiNaKanalu.push(vzdevkiGledeNaSocket[socket.id]);
    else{ //izpisejo se vsi vzdevki razen nasega
      for(var i = 0; i < uporabniki.length; i++){
          if(socket.id !== uporabniki[i].id)
            uporabnikiNaKanalu.push(vzdevkiGledeNaSocket[uporabniki[i].id]); //najdemo vzdevke in jih dodamo v tabelo 
      }
    }
    socket.emit('vzdevki',{ //posljemo nazaj uporabniku
      tab : uporabnikiNaKanalu,
    });
  
}
function obdelajZasebnoSporocilo(socket){
  socket.on('zasebnoSporocilo', function(sporocilo){
    //ne smemo posiljati samemu sebi!
    if(vzdevkiGledeNaSocket[socket.id] === sporocilo.nick){
      socket.emit('sporocilo', { //posljemo sporocilo pravemu uporabniku
          besedilo: 'Sporočila ' + sporocilo.message +' uporabniku z vzdevkom ' + sporocilo.nick + ' ni bilo mogoče posredovati.'
       });
    }
    else{
      var uspesnoPoslano = false;
      var uporabniki = io.sockets.clients();
      for(var i = 0; i < uporabniki.length; i++){
        //najprej dobimo socket in pogledamo v vzdevkeGledenaSocket kaksen vzdevek je--ce se sklada z nickom imamo pravi socket
        if(vzdevkiGledeNaSocket[uporabniki[i].id] === sporocilo.nick ){
          io.sockets.socket(uporabniki[i].id).emit('sporocilo', { //posljemo sporocilo pravemu uporabniku
            besedilo: vzdevkiGledeNaSocket[socket.id] + ' (zasebno): ' + sporocilo.message
          });
          socket.emit('sporocilo',{ //kar se prikaze posiljatelju
            besedilo:'(zasebno za ' +  sporocilo.nick + '): ' +sporocilo.message
          });
         uspesnoPoslano = true;
        break;
        }
      }
      if(!uspesnoPoslano){ //v primeru da je bil podan vzdevek napacen, oz. takega uporabnika ni na serverju
        socket.emit('sporocilo', { //posljemo sporocilo pravemu uporabniku
          besedilo: 'Sporočila \"' + sporocilo.message +'\" uporabniku z vzdevkom \"' + sporocilo.nick + '\" ni bilo mogoče posredovati.'
        });
      }
    }
  });

}