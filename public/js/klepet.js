var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal,passwd) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: passwd
  });
};


Klepet.prototype.osveziSeznamUporabnikov = function(){
  this.socket.emit('osveziUpor');
}

Klepet.prototype.posljiZasebnoSporocilo = function(vzdevek, sporocilo){
  var zasebno = {
    nick : vzdevek,
    message: sporocilo,
  };
  this.socket.emit('zasebnoSporocilo', zasebno);

}

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var celoSporocilo = ukaz;
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      var kanal;
      var passwd;
      if(celoSporocilo.search(/(".*?")/g) > -1){
        var orders = celoSporocilo.match(/(".*?")/g); //v orders spravimo kanal  in passwd
        kanal = orders[0];
        kanal = kanal.substring(1,kanal.length -1);
        passwd = orders[1];
        passwd = passwd.substring(1,passwd.length -1);
        this.spremeniKanal(kanal,passwd);
        console.log(kanal + ' ' + passwd);
      }
      else{
        besede.shift();
        passwd = null;
        kanal = besede.join(' ');
        this.spremeniKanal(kanal,passwd);
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      //besede.shift();
      var orders = celoSporocilo.match(/(".*?")/g); //v orders spravimo vzdevek in sporocilo
      var nick = orders[0];
      nick = nick.substring(1,nick.length-1);
      var message = orders[1];
      message = message.substring(1,message.length-1);
      console.log('nick: '+nick+', sporocilo:'+ message);
      this.posljiZasebnoSporocilo(nick,message);
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};