function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);

}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}
var buffer = {
  besede : [],
};
function procesirajVnosUporabnika(klepetApp, socket) {

  klepetApp.osveziSeznamUporabnikov();

  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    var besedilo;
    besedilo = filter(sporocilo);
    //ce najdemo kaksnega smeska:
    if(besedilo.indexOf(":)") > -1 || besedilo.indexOf(":(") > -1 || besedilo.indexOf(";)") > -1 || besedilo.indexOf(":*") > -1 || besedilo.indexOf("(y)") > -1){
      besedilo = zamenjajSmeske(besedilo); //nasli smo smeska/e in ga/jih gremo zamenjat
      $('#sporocila').append($(('<b>'+ besedilo  + '</b>' +  '<br>')));
    }
    else { //nismo nasli nic
      $('#sporocila').append(divElementEnostavniTekst(besedilo));
    }
    var napis = $('#kanal').text(); //kar je napisano na vrhu shranimo v spremenljivko napis
    var n = napis.indexOf('@'); //najdemo index @

    napis = napis.substring(n+2, napis.length); //ime kanala
    
    klepetApp.posljiSporocilo(napis, sporocilo);

    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var zamenjajSmeske = function(sporocilo){
  var str = preveri(sporocilo); //preverimo ce je bil poslan zlonameren script
  
  var n = str.indexOf(":)");
  if (n  !== -1){
    str = str.replace(/:\)/g, '<img src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"/>');
  }
  n = str.indexOf(";)"); 
  if(n !== -1){
    str = str.replace(/;\)/g, '<img src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"/>');
  }
  n = str.indexOf(":("); 
  if(n !== -1){
     str = str.replace(/:\(/g, '<img src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"/>');
  }
  n = str.indexOf("(y)");
  if(n !== -1){
    str = str.replace(/\(y\)/g, '<img src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"/>');
  }
  n = str.indexOf(":*");
  if(n !== -1){
    str = str.replace(/:\*/g, '<img src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png" />');
  }
  
  return str;
}

var preveri = function(sporocilo){
  var str = sporocilo;
  var n = str.indexOf("<");
  if(n !== -1)
    str = str.replace(/</g, '&lt');
  n = str.indexOf(">");
  if(n !== -1)
    str = str.replace(/>/g,'&gt');
    
  return str;
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      var napis = $('#kanal').text(); //kar je napisano na vrhu shranimo v spremenljivko napis
      var n = napis.indexOf('@'); //najdemo index @
     
       var prejsnjeIme = napis.substring(0,n); //v novo spremenljivko shranimo, kar je napisano v napisu pred @ -- ime
       napis = napis.replace(prejsnjeIme,rezultat.vzdevek + ' '); //zamenjamo z novim imenom
     
      $('#kanal').text(napis);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    grdeBesede();
    $('#kanal').text(rezultat.vzdevek + ' @ ' + rezultat.kanal);// 
    grdeBesede();
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });


  socket.on('sporocilo', function (message) {
    klepetApp.osveziSeznamUporabnikov();//ko prejmemo sporocilo moramo tudi osveziti seznam
    var sporocilo = message.besedilo;
    sporocilo = filter(sporocilo);
    var novElement; 
    //je smesko
    if(sporocilo.indexOf(":)") > -1 || sporocilo.indexOf(":(") > -1 || sporocilo.indexOf(";)") > -1 || sporocilo.indexOf(":*") > -1 || sporocilo.indexOf("(y)") > -1){
      novElement = zamenjajSmeske(sporocilo);
      $('#sporocila').append($(('<b>'+ novElement  + '</b>' +  '<br>')));
    }
    else{ //ni smeska
      novElement = $('<div style="font-weight: bold"></div>').text(sporocilo);  
      $('#sporocila').append(novElement);
    }
  
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();
    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('vzdevki', function(rezultat){
    $('#imena').empty(); //pobrisemo seznam uporabnikov
    var vsaImena = rezultat.tab;
    for(var i = 0; i < vsaImena.length;i++){
      if(vsaImena[i] === null) //za vsak slucaj
        continue;
      else 
        $('#imena').append(divElementHtmlTekst(vsaImena[i])); //dodajamo imena 
    }
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});


var grdeBesede = function(){ //nalaganje grdih besed iz xmlja v buffer
  var i = 0; 
   $(document).ready(function () {
    $.ajax({
        type: "GET",
        url: "swearWords.xml",
        dataType: "xml ",
        async : "false",
        
        success: function(xml) {
            $(xml).find('words').each(function(){
                $(xml).find('word').each(function(){
                var word = $(this).text();
                buffer.besede.push(word);
                i++;
                });
            });
                 
        },
        error: function(xml){
          alert("The XML File could not be processed correctly.");
        }
    }
    
    );
});
}

var filter = function(sporocilo) {
  for (var i = 0; i < buffer.besede.length; i++){ //gremo cez celoten array grdih besed
    var word = buffer.besede[i];
    var pojavitve;
    var regex = new RegExp("(^|\\s|[^A-Z ^a-z ^0-9])" + word + "($|\\s|[^A-Z ^a-z ^0-9])" , "i"); 
    var n = sporocilo.search(regex); //v besedilu je grda beseda
    if (n > -1) {
      var zakrito = '*';
      for(var k = 1; k < word.length;k++) 
        zakrito += '*';
        while(n > -1){
          pojavitve = sporocilo.match(regex);
          if(pojavitve[0] === word) //v besedilu je bila le ena beseda
            sporocilo = sporocilo.replace(regex,zakrito);
          else if (pojavitve[0].length === word.length+1){ //pobralo en znak vec
            if (pojavitve[0].charAt(0) !== word.charAt(0)) //in sicer ne na zacetku
              sporocilo = sporocilo.replace(regex,pojavitve[0].charAt(0)+zakrito);
            else //na zacetku
              sporocilo = sporocilo.replace(regex,zakrito+pojavitve[0].charAt(pojavitve[0].length-1));
          }
          else if(pojavitve[0].length === word.length+2){ //pobralo dva znaka vec)
              sporocilo = sporocilo.replace(regex,pojavitve[0].charAt(0)+zakrito+pojavitve[0].charAt(pojavitve[0].length-1));
          }
          else{
            break;
          } 
          n = sporocilo.search(regex);
        }
    }
 }
  return sporocilo;
}
